package com.demo;

import com.annotation.Autowired;
import com.annotation.Component;


@Component
public class TestController {
    @Autowired
    public UserService userService;
    
    
    public void getUser() {
        User user = userService.getUser();
        System.out.println(user);
    }
}