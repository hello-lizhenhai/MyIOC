package com.util;

import java.io.File;
import com.annotation.Component;
import com.ioc.IocContext;

public class AnnotationUtil {

    //获取指定包路径下实现 Component主键Bean的实例
    public static void addClassByAnnotation(String filePath, String packageName) {
        try {
            File[] files = ClassUtil.getClassFile(filePath);
            if (files != null) {
                for (File f : files) {
                    String fileName = f.getName();
                    if (f.isFile()) {
                        Class<?> clazz = Class.forName(packageName + "." + fileName.substring(0, fileName.lastIndexOf(".")));
                        //判断该类是否实现了注解
                        if(clazz.isAnnotationPresent(Component.class)) {
                          IocContext.applicationContext.put(clazz, clazz.newInstance());
                        }
                    } else {
                        addClassByAnnotation(f.getPath(), packageName + "." + fileName);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}