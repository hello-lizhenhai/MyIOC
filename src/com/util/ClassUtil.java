package com.util;

import java.io.File;
import java.io.FileFilter;

public class ClassUtil {

	/**
	 * 获取该路径下所遇的class文件和目录
	 * 
	 * @param pkg
	 * @return
	 */
	public static File[] getClassFile(String filePath) {
		return new File(filePath).listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isFile() && file.getName().endsWith(".class") || file.isDirectory();
			}
		});
	}

}
