package com;


import java.lang.reflect.Constructor;

import com.demo.TestController;
import com.demo.User;
import com.ioc.IocContext;

public class Main {
    public static void main(String[] args) throws Exception {
        TestController testController = (TestController)IocContext.applicationContext.get(TestController.class);
        testController.getUser();
        
        User user = User.class.newInstance();
        System.out.println(user);
       
        Constructor<User> constructor = User.class.getConstructor();
        System.out.println(constructor.isAccessible());
        User user2 = constructor.newInstance();
        System.out.println(user2);
        

    }
}