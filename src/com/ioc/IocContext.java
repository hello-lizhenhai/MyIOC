package com.ioc;

import java.net.URL;
import java.util.Enumeration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import com.util.AnnotationUtil;
/**
 * Ioc 容器实现类
 * 
 */
public class IocContext {
    public static final Map<Class<?>, Object> applicationContext = new ConcurrentHashMap<Class<?>, Object>();
    static{
    	// 指定需要扫描的包名
        String packageName = "com";
        try {
            initBean(packageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void initBean(String packageName) throws Exception {
        Enumeration<URL> urls = Thread.currentThread().getContextClassLoader().getResources(packageName.replaceAll("\\.", "/"));
        while (urls.hasMoreElements()) {
           AnnotationUtil.addClassByAnnotation(urls.nextElement().getPath(), packageName);
        }
        //IOC实现， 自定注入
        IocUtil.inject();
    }
  
}